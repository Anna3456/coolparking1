﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using System.Collections.ObjectModel;
using System.Collections.Generic;

public class ParkingService : IParkingService
{
    private Parking park = Parking.Instance;

    public ParkingService(ITimerService It, ITimerService Its, ILogService Il)
    {

    }
    public void AddVehicle(Vehicle vehicle)
    {
        List<Vehicle> vehicles = park.Vehicles;
    }

    public void Dispose()
    {
        
    }

    public decimal GetBalance()
    {
        return park.Balance;
    }

    public int GetCapacity()
    {
        throw new System.NotImplementedException();
    }

    public int GetFreePlaces()
    {
        throw new System.NotImplementedException();
    }

    public TransactionInfo[] GetLastParkingTransactions()
    {
        return new TransactionInfo[1]{
            new TransactionInfo()};
    }

    public ReadOnlyCollection<Vehicle> GetVehicles()
    {
        return park.Vehicles.AsReadOnly();
    }

    public string ReadFromLog()
    {
        throw new System.NotImplementedException();
    }

    public void RemoveVehicle(string vehicleId)
    {
        if(Vehicle.CheckId(vehicleId))
        {

        }
    }

    public void TopUpVehicle(string vehicleId, decimal sum)
    {
        throw new System.NotImplementedException();
    }
}