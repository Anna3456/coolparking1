﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found,
//       an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
public class LogService : ILogService
{
    public LogService(ILogService IL)
    {

    }

    public LogService(string filePath)
    {

    }
    public string LogPath => "";
    public string Read()
    {
        return "";
    }

    public void Write(string logInfo)
    {
    }
}
