﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Collections.Generic;

public class Vehicle : IComparer<Vehicle>
{
    private string ID;
    private VehicleType VehicleType;
    internal decimal balance;

    public string Id { get => ID;}
    public decimal Balance { get => balance; }

    public Vehicle()
    {
        ID = "AA-0000-AA";
        VehicleType = VehicleType.Bus;
        balance = 100;
    }

    public Vehicle(string id, VehicleType vehicleType, decimal balance)
    {
        if(!CheckId(id))
        {
            throw new ArgumentException();
        }
        ID = id;
        VehicleType = vehicleType;
        balance = balance;
    }

    public static int GenerateRandomRegistrationPlateNumber()
    { 
        var rand = new Random(DateTime.Now.Millisecond);
        return rand.Next();
    }

    public static bool CheckId(string id)
    {
        if (id.Length != 10)
        {
            return false;
        }
        else if (!(Char.IsUpper(id, 0) && Char.IsUpper(id, 1) && id[2] == '-' && Char.IsDigit(id, 3) &&
            Char.IsDigit(id, 4) && Char.IsDigit(id, 5) && Char.IsDigit(id, 6) && id[7] == '-' &&
            Char.IsUpper(id, 8) && Char.IsUpper(id, 9)))
        {
            return false;
        }
        return true;
    }

    public int Compare(Vehicle x, Vehicle y)
    {
        if (x.ID.CompareTo(y.ID) != 0)
            return x.ID.CompareTo(y.ID);
        return 0;
    }

    public void RefillBalance(decimal newBalance)
    {
        balance += newBalance;
    }

}