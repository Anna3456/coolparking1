﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;
using System;
public sealed class Parking
{
    private List<Vehicle> vehicles;
    internal decimal Balance;

    private static readonly Lazy<Parking> _instance = new Lazy<Parking>(() => new Parking());
    Parking()
    {
        vehicles = new List<Vehicle>();
    }
    public static Parking Instance { get { return _instance.Value; } }
    internal List<Vehicle> Vehicles { get => vehicles; set => vehicles = value; }

    internal void AddVehicle(Vehicle vehicle)
    {
        vehicles.Add(vehicle);
    }

    internal void RemoveVehicles(string Id)
    {
        int index = vehicles.FindIndex(c => c.Id == Id);
        vehicles.RemoveAt(index);
    }
}