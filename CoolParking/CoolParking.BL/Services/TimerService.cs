﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

class TimerService : ITimerService
{
    private Timer timer;
    public double Interval { get => timer.Interval; set => timer.Interval = value; }

    public event ElapsedEventHandler Elapsed;

    public void Dispose()
    {
        timer.Dispose();
    }

    public void Start()
    {
        timer.Start();
    }

    public void Stop()
    {
        timer.Stop();
    }
}