﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
public struct TransactionInfo
{
    DateTime Time;
    string Id;
    decimal Money;
    decimal Sum(Func<TransactionInfo, decimal> func)
    {
        return func(this);
    }
}